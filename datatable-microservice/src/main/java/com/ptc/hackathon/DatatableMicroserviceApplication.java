package com.ptc.hackathon;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import com.ptc.hackathon.mbps.jdbc.DataTableDao;
import com.ptc.hackathon.mbps.jpa.GenericJpaRepository;
import com.ptc.hackathon.mbps.utils.DBUtils;

@SpringBootApplication
public class DatatableMicroserviceApplication implements CommandLineRunner {

	@Autowired
	public DataTableDao dao;
	
	@Autowired
	public DBUtils dbUtils;
	
	@Autowired
	public GenericJpaRepository jpaRepository;
	
	public static ApplicationContext appContext;
	
	protected static final Logger LOG = LoggerFactory.getLogger(DatatableMicroserviceApplication.class);
	public static void main(String[] args) {
		appContext = SpringApplication.run(DatatableMicroserviceApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("application started");
		
			try {
				 
				Scanner scanner = new Scanner(this.getClass().getClassLoader().getResourceAsStream("ApplicationStartedText.txt"));
				while (scanner.hasNextLine()) {
					System.out.println(scanner.nextLine());
				}
				scanner.close();
			} catch (Exception e) {
				e.printStackTrace();
			}
	}




}
