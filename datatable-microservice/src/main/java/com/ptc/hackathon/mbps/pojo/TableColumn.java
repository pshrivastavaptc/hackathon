package com.ptc.hackathon.mbps.pojo;

public class TableColumn {

	public String getColumnName() {
		return columnName;
	}
	public void setColumnName(String columnName) {
		this.columnName = columnName;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public Boolean getIsPrimary() {
		return isPrimary;
	}
	public void setIsPrimary(Boolean isPrimary) {
		this.isPrimary = isPrimary;
	}
	public TableColumn(String columnName, String type, Boolean isPrimary) {
		super();
		this.columnName = columnName;
		this.type = type;
		this.isPrimary = isPrimary;
	}
	private String columnName;
	private String type;
	private Boolean isPrimary;
	
	public String toString(boolean commitForm) {
		if(!commitForm){
			return "TableColumn [columnName=" + columnName + ", type=" + type + ", isPrimary=" + isPrimary + "]";
		}
		else{
			return columnName+":"+type+";";
		}
	}
	
	@Override
	public boolean equals(Object obj) {
		if(obj instanceof TableColumn) {
			TableColumn toCompare = (TableColumn) obj;
			return toCompare.getIsPrimary().equals(this.getIsPrimary()) && toCompare.getType().equalsIgnoreCase(this.getType())
					&& toCompare.getColumnName().equalsIgnoreCase(this.getColumnName());
		}
		return false;
	}
	
	
}
