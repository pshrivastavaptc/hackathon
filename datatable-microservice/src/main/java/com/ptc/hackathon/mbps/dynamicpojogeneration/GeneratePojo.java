package com.ptc.hackathon.mbps.dynamicpojogeneration;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.Map.Entry;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;

public class GeneratePojo {

	public static CtClass generate(String className, Map<String, String> properties)
			throws NotFoundException, CannotCompileException, IOException, ClassNotFoundException {

		try {
            Class.forName(className, false, GeneratePojo.class.getClassLoader());
        } catch (ClassNotFoundException e) {
        	ClassPool pool = ClassPool.getDefault();
    		CtClass cc= pool.makeClass(className);
    		cc.addInterface(resolveCtClass(Serializable.class));

    		
    		ClassFile cFile = cc.getClassFile();
    		ConstPool constpool = cFile.getConstPool();

    		AnnotationsAttribute attr = new AnnotationsAttribute(constpool, AnnotationsAttribute.visibleTag);
    		Annotation annot = new Annotation("javax.persistence.Entity", constpool);
    		attr.addAnnotation(annot);
    		cFile.addAttribute(attr);

    		for (Entry<String, String> entry : properties.entrySet()) {
    			cc.addField(new CtField(resolveCtClass(entry.getValue().getClass()), entry.getKey(), cc));
    			CtMethod mthd = generateGetter(cc, entry.getKey(), entry.getValue().getClass());
    			// add getter
    			cc.addMethod(mthd);
    			mthd = generateSetter(cc, entry.getKey(), entry.getValue().getClass());
    			// add setter
    			cc.addMethod(mthd);
    		}
    		return cc;
        }
		return null;
		
		
	}

	private static CtMethod generateGetter(CtClass declaringClass, String fieldName, Class fieldClass)
			throws CannotCompileException {

		String getterName = "get" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

		StringBuffer sb = new StringBuffer();
		sb.append("public ").append(fieldClass.getName()).append(" ").append(getterName).append("(){")
				.append("return this.").append(fieldName).append(";").append("}");
		return CtMethod.make(sb.toString(), declaringClass);
	}

	private static CtMethod generateSetter(CtClass declaringClass, String fieldName, Class fieldClass)
			throws CannotCompileException {

		String setterName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);

		StringBuffer sb = new StringBuffer();
		sb.append("public void ").append(setterName).append("(").append(fieldClass.getName()).append(" ")
				.append(fieldName).append(")").append("{").append("this.").append(fieldName).append("=")
				.append(fieldName).append(";").append("}");
		return CtMethod.make(sb.toString(), declaringClass);
	}

	private static CtClass resolveCtClass(Class clazz) throws NotFoundException {
		ClassPool pool = ClassPool.getDefault();
		return pool.get(clazz.getName());
	}

}
