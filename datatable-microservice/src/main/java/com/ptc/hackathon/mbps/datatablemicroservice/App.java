package com.ptc.hackathon.mbps.datatablemicroservice;

import java.util.Date;

import org.hibernate.Session;

import com.ptc.hackathon.mbps.utils.HibernateUtil;


public class App {
	public static void main(String[] args) {
		System.out.println("Maven + Hibernate + Oracle");
		Session session = HibernateUtil.getSessionFactory().openSession();

		session.beginTransaction();
		DBUser user = new DBUser();

		user.setUserId(100);
		user.setUsername("superman");
		user.setCreatedBy("system");
		user.setCreatedDate(new Date());

		System.out.println("Getting the session");
		session.save(user);
		System.out.println("Commiting the transaction");
		session.getTransaction().commit();
	}
}