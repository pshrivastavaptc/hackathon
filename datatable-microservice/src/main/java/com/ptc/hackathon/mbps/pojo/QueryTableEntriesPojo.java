package com.ptc.hackathon.mbps.pojo;

public class QueryTableEntriesPojo {

	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getQuery() {
		return query;
	}
	public void setQuery(String query) {
		this.query = query;
	}
	public QueryTableEntriesPojo(String tableName, String dataShapeName) {
		super();
		this.tableName = tableName;
		this.query = dataShapeName;
	}
	public QueryTableEntriesPojo () {}
	private String tableName;
	private String query;
}
