package com.ptc.hackathon.mbps.dynamicpojogeneration;


import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.Method;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONObject;

import com.ptc.hackathon.mbps.pojo.generated.GeneratedClass;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtField;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.bytecode.AnnotationsAttribute;
import javassist.bytecode.ClassFile;
import javassist.bytecode.ConstPool;
import javassist.bytecode.annotation.Annotation;

public class PojoGenerator {


/*	public static CtClass generate(String className, Map<String, Class<?>>  properties) throws NotFoundException,
			CannotCompileException, IOException {


		ClassPool pool = ClassPool.getDefault();
		CtClass cc = pool.makeClass(className);
		cc.addInterface(resolveCtClass(Serializable.class));

		for (Entry<String, Class<?>> entry : properties.entrySet()) {

			
			cc.addField(new CtField(resolveCtClass(entry.getValue()), entry.getKey(), cc));

			CtMethod mthd = generateGetter(cc, entry.getKey(), entry.getValue());
			// add getter
			cc.addMethod(mthd);
			
			
			ClassFile cFile = cc.getClassFile();
			ConstPool constpool = cFile.getConstPool();
			
			AnnotationsAttribute attr = new AnnotationsAttribute(constpool, AnnotationsAttribute.visibleTag);
			Annotation annot = new Annotation("javax.persistence.Column", constpool);
			attr.addAnnotation(annot);
			mthd.getMethodInfo().addAttribute(attr);
			// to add annotation to the class
			//cFile.addAttribute(attr);
			System.out.println("Annotation length");
			System.out.println(mthd.getAnnotations().length);
			mthd = generateSetter(cc, entry.getKey(), entry.getValue());
			// add setter
			cc.addMethod(mthd);		
			
		}
		
		
		
		return cc;
	}*/
	public static final String GENERATED_CLASSES_PACKAGE = "com.ptc.hackathon.";
	public static Map<String, Class> twxTypeToJavaType = new HashMap<String, Class>();
	static {
		twxTypeToJavaType.put("STRING",String.class);
		twxTypeToJavaType.put("INTEGER",Integer.class);
		twxTypeToJavaType.put("NUMBER",Double.class);
		twxTypeToJavaType.put("DATETIME",Date.class);
		twxTypeToJavaType.put("JSON",JSONObject.class);
	}
	public static CtClass generate(String className, Map<String, String> properties)
			throws NotFoundException, CannotCompileException, IOException {

		ClassPool pool = ClassPool.getDefault();
		CtClass cc = null;
		try{
		  cc = pool.get(className);
		  return cc;
		}catch(Exception e){
			cc = pool.makeClass(className);
		}
		cc.addInterface(resolveCtClass(Serializable.class));
		cc.addInterface(resolveCtClass(GeneratedClass.class));
		for (Entry<String, String> entry : properties.entrySet()) {

			Class propertyType = twxTypeToJavaType.get(entry.getValue());
			cc.addField(new CtField(resolveCtClass(propertyType), entry.getKey(), cc));

			// add getter
			cc.addMethod(generateGetter(cc, entry.getKey(), propertyType));

			// add setter
			cc.addMethod(generateSetter(cc, entry.getKey(), propertyType));

		}

		return cc;
	}	

	private static CtMethod generateGetter(CtClass declaringClass, String fieldName, Class fieldClass)
			throws CannotCompileException {

		String getterName = "get" + fieldName.substring(0, 1).toUpperCase()
				+ fieldName.substring(1);

		StringBuffer sb = new StringBuffer();
		sb.append("public ").append(fieldClass.getName()).append(" ")
				.append(getterName).append("(){").append("return this.")
				.append(fieldName).append(";").append("}");
		return CtMethod.make(sb.toString(), declaringClass);
	}

	private static CtMethod generateSetter(CtClass declaringClass, String fieldName, Class fieldClass)
			throws CannotCompileException {

		String setterName = "set" + fieldName.substring(0, 1).toUpperCase()
				+ fieldName.substring(1);

		StringBuffer sb = new StringBuffer();
		sb.append("public void ").append(setterName).append("(")
				.append(fieldClass.getName()).append(" ").append(fieldName)
				.append(")").append("{").append("this.").append(fieldName)
				.append("=").append(fieldName).append(";").append("}");
		return CtMethod.make(sb.toString(), declaringClass);
	}

	private static CtClass resolveCtClass(Class clazz) throws NotFoundException {
		ClassPool pool = ClassPool.getDefault();
		return pool.get(clazz.getName());
	}
	

public static void main(String[] args) throws Exception {

	Map<String, String> props = new HashMap<String, String>();
	props.put("foo", "INTEGER");
	props.put("bar", "STRING");

	CtClass cc = PojoGenerator.generate(
			"net.javaforge.blog.javassist.Pojo$Generated", props);
	 Class <?> clazz = cc.toClass();
	 
	 //java.lang.annotation.Annotation[] annots = clazz.getAnnotations();
	// System.out.println(annots.length);
	Object obj = clazz.newInstance();
	byte[] classByte = cc.toBytecode();
	
	
	
for (final Method method : clazz.getDeclaredMethods()) {
		System.out.println(method);
		java.lang.annotation.Annotation[] annotss = method.getAnnotations();
		System.out.println(annotss.length);
	}
/*
	// set property "bar"
	clazz.getMethod("setBar", String.class).invoke(obj, "Hello World!");

	// get property "bar"
	String result = (String) clazz.getMethod("getBar").invoke(obj);
	System.out.println("Value for bar: " + result);
*/
}
}


