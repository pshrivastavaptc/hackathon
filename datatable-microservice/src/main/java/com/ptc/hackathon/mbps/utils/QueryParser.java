package com.ptc.hackathon.mbps.utils;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class QueryParser {

	private static final Logger logger = LoggerFactory.getLogger(QueryParser.class);

	public static final String FILTERS = "filters";
	public static final String SORTS = "sorts";
	public static final String TYPE = "type";
	public static final String FILEDNAME = "fieldName";
	public static final String EXPRESSION = "expression";
	public static final String VALUE = "value";
	public static final String VALUES = "values";
	public static final String FROM = "from";
	public static final String TO = "to";
	public static final String ISASCENDING = "isAscending";
	public static final String ISCASESENSITIVE = "isCaseSensitive";
	public static final String ASC = "ASC";
	public static final String DESC = "DESC";
	public static final String COLLATE_NOCASE = "COLLATE NOCASE";
	public static Map<String, String> filterQueryTypes = new HashMap<String, String>();

	JSONObject _query = null;

	public QueryParser(JSONObject query) {
		_query = query;
		init();
	}

	private void init() {
		filterQueryTypes.put(MicroServiceConstants.GT, ">");
		filterQueryTypes.put(MicroServiceConstants.LT, "<");
		filterQueryTypes.put(MicroServiceConstants.EQ, "=");
		filterQueryTypes.put(MicroServiceConstants.LE, "<=");
		filterQueryTypes.put(MicroServiceConstants.GE, ">=");
		filterQueryTypes.put(MicroServiceConstants.NE, "<>");
		filterQueryTypes.put(MicroServiceConstants.LIKE, "LIKE");
		filterQueryTypes.put(MicroServiceConstants.NOTLIKE, "NOT LIKE");
		filterQueryTypes.put(MicroServiceConstants.MISSINGVALUE, "IS NULL");
		filterQueryTypes.put(MicroServiceConstants.NOTMISSINGVALUE, "IS NOT NULL");
		filterQueryTypes.put(MicroServiceConstants.MATCHES, "LIKE");
		filterQueryTypes.put(MicroServiceConstants.NOTMATCHES, "NOT LIKE");
		filterQueryTypes.put(MicroServiceConstants.IN, "IN");
		filterQueryTypes.put(MicroServiceConstants.NOTIN, "NOT IN");
	}

	public String getParsedQuery() {
		JSONObject filterQuery = _query.optJSONObject(FILTERS);
		JSONArray sortQuery = _query.optJSONArray(SORTS);
		String sqlString = "";
		try {
			if (filterQuery != null) {
				String filterType = filterQuery.optString(TYPE).toUpperCase();
				switch (filterType) {
				case MicroServiceConstants.AND:
				case MicroServiceConstants.OR:
					sqlString = getFilteredComplexParsedQuery(filterQuery.optJSONArray(FILTERS), filterType);
					break;
				default:
					sqlString = getFilteredParsedQuery(filterQuery, filterType);
				}
			}
			if (sortQuery != null) {
				sqlString += getSortParsedQuery(sortQuery);
			}
		} catch (Exception e) {
			logger.error("Unable to parse query");
		}
		if (sqlString.isEmpty()) {
			return sqlString;
		} else {
			return " where " + sqlString;
		}
	}

	private String getSortParsedQuery(JSONArray sortQuery) {
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < sortQuery.length(); i++) {
			JSONObject childSortQuery = sortQuery.optJSONObject(i);
			String fieldName = childSortQuery.optString(FILEDNAME);
			Boolean isAscending = childSortQuery.optBoolean(ISASCENDING);
			Boolean isCaseSensitive = childSortQuery.optBoolean(ISCASESENSITIVE);
			if (fieldName != null) {
				if (i == 0)
					sql.append(" ").append("ORDER BY ");
				sql.append(fieldName);
				if (isCaseSensitive)
					sql.append(" ").append(COLLATE_NOCASE);
				if (isAscending)
					sql.append(" ").append(ASC);
				else
					sql.append(" ").append(DESC);
			}
			if (sortQuery.length() == i + 1)
				continue;
			else
				sql.append(", ");
		}
		return sql.toString();
	}

	private String getFilteredComplexParsedQuery(JSONArray filterQuery, String filterType) throws JSONException {
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < filterQuery.length(); i++) {
			JSONObject childfilterQuery = filterQuery.optJSONObject(i);
			String childfilterType = childfilterQuery.optString(TYPE);
			sql.append(getFilteredParsedQuery(childfilterQuery, childfilterType));
			if (filterQuery.length() == i + 1)
				continue;
			else
				sql.append(" ").append(filterType).append(" ");
		}
		return sql.toString();
	}

	private String getFilteredParsedQuery(JSONObject filterQuery, String filterType) throws JSONException {
		String fieldName = filterQuery.optString(FILEDNAME);
		filterType = filterType.toUpperCase();
		StringBuilder sql = new StringBuilder();
		switch (filterType) {
		case MicroServiceConstants.GT:
		case MicroServiceConstants.LT:
		case MicroServiceConstants.LE:
		case MicroServiceConstants.GE:
		case MicroServiceConstants.NE:
		case MicroServiceConstants.EQ:
		case MicroServiceConstants.LIKE:
		case MicroServiceConstants.NOTLIKE:
			Object value = filterQuery.get(VALUE);
			sql.append(fieldName).append(" ").append(filterQueryTypes.get(filterType)).append(" ");
			StringBuilder valueBuilder = new StringBuilder();
			if(value instanceof String) {
				valueBuilder.append("'").append(value).append("'");
				sql.append(valueBuilder.toString());
			} else {
				sql.append(value);
			}
			
			break;
		case MicroServiceConstants.IN:
		case MicroServiceConstants.NOTIN:
			JSONArray values = filterQuery.optJSONArray(VALUES);
			String sqlValues = getValuesFromJSONArray(values);
			sql.append(fieldName).append(" ").append(filterQueryTypes.get(filterType)).append(" ").append(sqlValues);
			break;
		case MicroServiceConstants.BETWEEN:
		case MicroServiceConstants.NOTBETWEEN:
			String from = filterQuery.optString(FROM);
			String to = filterQuery.optString(TO);
			sql.append(fieldName).append(" ").append(filterType).append(" ").append(from).append(" ").append("AND")
					.append(" ").append(to);
			break;
		case MicroServiceConstants.MISSINGVALUE:
		case MicroServiceConstants.NOTMISSINGVALUE:
			sql.append(fieldName).append(" ").append(filterQueryTypes.get(filterType)).append(" ");
			break;
		case MicroServiceConstants.MATCHES:
		case MicroServiceConstants.NOTMATCHES:
			String strExpression = filterQuery.getString(EXPRESSION);
			JSONArray expression = new JSONArray(strExpression);
			sql.append(fieldName).append(getValuesFromJSONArray(expression, filterType, fieldName));
			break;

		}
		return sql.toString();

	}

	private String getValuesFromJSONArray(JSONArray expression, String filterType, String fieldName) {
		StringBuilder sql = new StringBuilder();
		for (int i = 0; i < expression.length(); i++) {
			sql.append(" ").append(filterQueryTypes.get(filterType)).append(" '%").append((expression.opt(i)))
					.append("%'");
			if (expression.length() == i + 1)
				continue;
			else
				sql.append(" OR " + fieldName);
		}
		return sql.toString();
	}

	private String getValue(Object o) {
		StringBuilder sql = new StringBuilder();
		if (o instanceof String)
			sql.append("'").append(o.toString()).append("'");
		else
			sql.append(o.toString());
		return sql.toString();
	}

	private String getValuesFromJSONArray(JSONArray values) {
		StringBuilder sql = new StringBuilder("(");
		for (int i = 0; i < values.length(); i++) {
			sql.append(getValue(values.opt(i)));
			if (values.length() == i + 1)
				continue;
			else
				sql.append(",");
		}
		sql.append(")");
		return sql.toString();
	}
}
