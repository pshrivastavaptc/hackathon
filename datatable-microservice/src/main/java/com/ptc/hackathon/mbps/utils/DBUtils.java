package com.ptc.hackathon.mbps.utils;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import com.ptc.hackathon.mbps.dynamicpojogeneration.PojoGenerator;
import com.ptc.hackathon.mbps.jdbc.DataTableDao;
import com.ptc.hackathon.mbps.jpa.GenericJpaRepository;
import com.ptc.hackathon.mbps.pojo.DataTableFields;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.pojo.TableDelta;
import com.ptc.hackathon.mbps.pojo.generated.GeneratedClass;
import com.ptc.hackathon.mbps.thingworx_rest.RestClient;

@Service
public class DBUtils {

	@Autowired
	private RestClient restClient;	
	@Autowired
	private DataTableDao dao;
	
	@Autowired
	private GenericJpaRepository jpaRepository;
	
	private static final String DATATABLE_FIELDS = "DATA_TABLE_FIELDS";
	private static final Logger LOG = LoggerFactory.getLogger(DBUtils.class);
	private static final String DATASHAPES = "DataShapes";
	private static final String URL_DELIMITER = "/";
	private static final String FIELD_DEFINITIONS = "FieldDefinitions";
	public static Map<String, String> baseTypesToDBType = new HashMap();

	public static final String createTableSql = "CREATE TABLE ";
	public static final String primaryKey = "primary key";
	public static final String insertIntoSql = "INSERT INTO ";
	public static final String COUNT_QUERY_BASIC = "select count(*) from ";
	public static final String VALUES = "values";
	public static final String selectAllFrom = "select * from ";
	public static final String WHERE = "where ";
	public static final String DROP_TABLE_SQL = "DROP TABLE "; 
	private static final List<TableColumn> defaultColumnsList = new ArrayList<TableColumn>();
	private static final String SHOW_COLUMNS_FROM = "SHOW COLUMNS FROM ";
	private static final String UPDATE_SQL = "UPDATE ";
	private static final String SET = "SET ";
	
	private static final String ALTER = "ALTER ";
	private static final String ADD = "ADD ";
	private static final String DROP = "DROP ";
	private static final String TABLE = "TABLE ";
	private static final String COLUMN = "COLUMN ";
	
	
	static {
		baseTypesToDBType.put("INTEGER", "integer");
		baseTypesToDBType.put("STRING", "varchar(255)");
		baseTypesToDBType.put("BOOLEAN", "boolean");
		baseTypesToDBType.put("NUMBER", "number");
		baseTypesToDBType.put("DATETIME", "timestamp");
		baseTypesToDBType.put("JSON", "blob");
		
		defaultColumnsList.add(new TableColumn("key","STRING", false));
		defaultColumnsList.add(new TableColumn("source","STRING", false));
		defaultColumnsList.add(new TableColumn("sourceType","STRING", false));
		defaultColumnsList.add(new TableColumn("location","STRING", false));
		defaultColumnsList.add(new TableColumn("tags","STRING", false));
		defaultColumnsList.add(new TableColumn("timestamp","DATETIME", false));
	}

	public String obtainDataShapeJson(String dataShapeName) throws Exception {
		String url = DATASHAPES + URL_DELIMITER + dataShapeName + URL_DELIMITER + FIELD_DEFINITIONS;
		return restClient.thingWorxGet(url);
	}

	public TableColumn[] obtainColumnsFromDataShape(JSONObject dataShape) throws JSONException {
		JSONArray rows = dataShape.getJSONArray("rows");
		JSONObject field = null;
		TableColumn[] columns = new TableColumn[rows.length()];
		for (int i = 0; i < rows.length(); i++) {
			field = rows.getJSONObject(i);
			columns[i] = new TableColumn(field.getString("name"), field.getString("baseType"),
					field.has("isPrimaryKey"));
		}
		return columns;

	}

	public String createDataTableSql(Table table) {
		//TODO add the constraints to tableColumn pojo and include them too while creating the tables.
		StringBuilder sql = new StringBuilder(createTableSql);
		String name = table.getTableName();
		sql.append(name).append(" (");
		TableColumn[] columns = table.getColumns();
		List<String> primaryKeyColumns = new ArrayList<String>();
		for (TableColumn column : columns) {
			sql.append(column.getColumnName()).append(" ").append(baseTypesToDBType.get(column.getType())).append(",");
			if (column.getIsPrimary()) {
				primaryKeyColumns.add(column.getColumnName());
			}

		}
		sql.append(primaryKey).append("(").append(primaryKeyColumns.get(0)).append(")");
		sql.append(")");
		return sql.toString();
	}

	public String obtainParsedQuerySQL(JSONObject query, String dataTableName) {
		StringBuilder sql = new StringBuilder(selectAllFrom);
		QueryParser qp = new QueryParser(query);
		sql.append(dataTableName).append(qp.getParsedQuery());
		return sql.toString();
	}

	public JdbcTemplate obtainJdbcTemplate() {
		if(this.dao != null) {
			return dao.getJdbcTemplate();
		}
		Object dao = com.ptc.hackathon.DatatableMicroserviceApplication.appContext.getBean("DataTableDao");
		if(dao instanceof DataTableDao) {
			return ((DataTableDao) dao).getJdbcTemplate();
		}
		LOG.error("bean with name {} not found", "jdbcTemplate");
		return null;
	}
	
	/**
	 * Method to validate that the table DataTableFields exists or not. If it does not exist, creates it.
	 */
	public void checkAndCreateDataTableFieldsTable() throws Exception {
		boolean isPresent = checkTableExists(DATATABLE_FIELDS);
		if(!isPresent) {
			TableColumn[] columns = new TableColumn[3];
			columns[0] = new TableColumn("data_table_name", "STRING", true);
			columns[1] = new TableColumn("data_shape_name", "STRING", false);
			columns[2] = new TableColumn("fields","JSON",false);
			Table table = new Table(DATATABLE_FIELDS, columns);
			String createTableSql = createDataTableSql(table);
			dao.executeSql(createTableSql);
		}
	}
	
	public boolean checkTableExists(String tableName) throws Exception {
		String h2SchemaTableName = dao.getTableNameForSchema();
		String h2SchemaColumnForTableName = dao.getColumnForTableNameSchema();
		tableName = tableName.toUpperCase();
		String sql = COUNT_QUERY_BASIC + h2SchemaTableName + " WHERE " + h2SchemaColumnForTableName + "='" + tableName+"'";
		return dao.getCountResult(sql, h2SchemaTableName)>0;
	}
	
	public DataTableFields commitDataTableFields(String tableName, String dataShapeName, TableColumn[] columns) {
		DataTableFields dtf = new DataTableFields();
		dtf.setDataTableName(tableName);
		dtf.setDataShapeName(dataShapeName);	
		StringBuilder sbuild = new StringBuilder();
		for(TableColumn column : columns){
			sbuild.append(column.toString(true));
			if(column.getIsPrimary()) {
				dtf.setPrimaryKeyField(column.getColumnName());
			}
		}
		dtf.setFields(sbuild.toString());	
		jpaRepository.update(dtf);
		return dtf;
	}
	
	public void addDefaultColumnsToTable (Table table) {
		ArrayList<TableColumn> finalColumns = new ArrayList<TableColumn>( Arrays.asList(table.getColumns()));
		finalColumns.addAll(defaultColumnsList);		
		table.setColumns(finalColumns.toArray(new TableColumn[finalColumns.size()]));
	}
	
	public StringBuilder addDataTableEntriesSql(String dataTableName, Map<String, String> nameToTypeMap) {
		
		StringBuilder sql = new StringBuilder("Insert into ");
		sql.append(dataTableName);
		sql.append("(");
		StringBuilder col = new StringBuilder();
		StringBuilder values = new StringBuilder("VALUES (");
		
		for(String keys : nameToTypeMap.keySet()) {
			col.append(keys);
			values.append("?");
			col.append(",");
			values.append(",");
			
		}
		
		String str = col.substring(0, col.length() -1);
		col = new StringBuilder(str).append(")");
		String str2 = values.substring(0, values.length() -1);
		values = new StringBuilder(str2);
		values.append(")");
		sql.append(col);
		sql.append(values);
		return sql;
	}

		public GeneratedClass findById(String tableName, Object key) throws Exception {
		StringBuilder sbuild = new StringBuilder();
		DataTableFields dtf = jpaRepository.findById(tableName);
		sbuild.append(selectAllFrom).append(" ").append(tableName).append(" ").append(WHERE).append(" ")
				.append(dtf.getPrimaryKeyField()).append(" = '").append(key.toString()).append("'");
		Class generatedClass = null;
		try{
			generatedClass = Class.forName(PojoGenerator.GENERATED_CLASSES_PACKAGE+tableName);
		
		} catch (ClassNotFoundException e) {
			Map<String, String> nameToTypeMap = Utilities.convertFieldsToMap(dtf);
			
			generatedClass = PojoGenerator.generate(
					PojoGenerator.GENERATED_CLASSES_PACKAGE+tableName, nameToTypeMap).toClass(this.getClass().getClassLoader(), this.getClass().getProtectionDomain());
		}
		return dao.findById(sbuild.toString(), generatedClass);
	}
	
	public List<GeneratedClass> findEntries(String sql, String tableName) throws Exception {
		DataTableFields dtf = jpaRepository.findById(tableName);
		Class generatedClass = null;
		try{
			generatedClass = Class.forName(PojoGenerator.GENERATED_CLASSES_PACKAGE+tableName);
		
		} catch (ClassNotFoundException e) {
			Map<String, String> nameToTypeMap = Utilities.convertFieldsToMap(dtf);
			
			generatedClass = PojoGenerator.generate(
					PojoGenerator.GENERATED_CLASSES_PACKAGE+tableName, nameToTypeMap).toClass();
		}
		return dao.query(sql, generatedClass);
	}
	
	public void removeDataTable(String dataTableName) {
		StringBuilder sbuild = new StringBuilder();
		sbuild.append(DROP_TABLE_SQL).append(dataTableName);
		dao.executeSql(sbuild.toString());
	}
	
	public void handleTableUpdate(Table newTable) {
		Table oldTable = obtainCurrentTable(newTable.getTableName());
		TableDelta delta = Utilities.calculateTableDelta(oldTable, newTable);
		
		for(TableColumn column : delta.getAddedColumns()) {
			StringBuilder addColumnSQlBuilder = new StringBuilder();
			addColumnSQlBuilder.append(ALTER).append(TABLE).append(newTable.getTableName()).append(" ").append(ADD);
			addColumnSQlBuilder.append(column.getColumnName()).append(" ")
					.append(baseTypesToDBType.get(column.getType()));
			dao.executeSql(addColumnSQlBuilder.toString());
		}
		
		for(TableColumn column : delta.getRemovedColumns()) {
			StringBuilder removeColumnSQlBuilder = new StringBuilder();
			removeColumnSQlBuilder.append(ALTER).append(TABLE).append(newTable.getTableName()).append(" ").append(DROP)
					.append(COLUMN);
			removeColumnSQlBuilder.append(column.getColumnName());
			dao.executeSql(removeColumnSQlBuilder.toString());
		}
		
		for(TableColumn column : delta.getModifiedColumns()) {
			StringBuilder modifyColumnSQLBuilder = new StringBuilder();
			modifyColumnSQLBuilder.append(ALTER).append(TABLE).append(newTable.getTableName()).append(" ").append(ALTER)
					.append(COLUMN).append(column.getColumnName()).append(" ")
					.append(baseTypesToDBType.get(column.getType()));
			dao.executeSql(modifyColumnSQLBuilder.toString());
		}
	}
	
	public Table obtainCurrentTable(String tableName) {
		Table retVal = null;
		String sqlForDesc = SHOW_COLUMNS_FROM + tableName;
		List<TableColumn> columnsList = dao.obtainTableColumnsFromDB(sqlForDesc, tableName);
		retVal =new Table(tableName, columnsList.toArray(new TableColumn[columnsList.size()]));
		return retVal;
	}
	
	public void updateEntry(String tableName, JSONObject updatedObject) throws Exception {
		dao.executeSql(updateEntrySql(tableName, updatedObject));
	}
	
	public String updateEntrySql(String tableName, JSONObject updatedObject) throws Exception {
		DataTableFields dtf = jpaRepository.findById(tableName);
		String primaryKeyField = dtf.getPrimaryKeyField();
		
		if(!(updatedObject.has("key") || updatedObject.has(primaryKeyField))) {
			throw new Exception("Primary key is required");
		}
		
		StringBuilder sqlBuilder = new StringBuilder();
		sqlBuilder.append(UPDATE_SQL).append(tableName).append(" ").append(SET).append(" ");
		Iterator<String> keyIterator = updatedObject.keys();
		while(keyIterator.hasNext()) {
			String field = keyIterator.next();
			if(field.equals(primaryKeyField)){
				continue;
			}
			sqlBuilder.append(field).append("=");
			Object value = updatedObject.get(field);
			if(value instanceof String){
				sqlBuilder.append("'").append(value).append("'");
			} else if ("DATETIME".equals(dtf.typeOfField(field))) {
				Long dateLong = (Long)value;
				String datePattern = "yyyy-MM-dd HH:mm:ss";
				SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
				value = new Date(dateLong);
//				String ;
				sqlBuilder.append("PARSEDATETIME(").append(sdf.format(value)).append(")");
			}else {
				sqlBuilder.append(value);
			}
			sqlBuilder.append(",");
		}

		String sqlString = sqlBuilder.toString();
		sqlString = sqlString.substring(0, sqlString.length()-1);
		sqlBuilder = new StringBuilder(sqlString);
		sqlBuilder.append(" ").append(WHERE);
		if(updatedObject.has(primaryKeyField)){
			sqlBuilder.append(primaryKeyField).append("=").append("'").append(updatedObject.get(primaryKeyField)).append("'");
		}
		return sqlBuilder.toString();
	}
	
	public boolean checkKeyExists(String tableName, Object key) throws Exception {
		StringBuilder sqlBuilder = new StringBuilder();
		DataTableFields dtf = jpaRepository.findById(tableName);
		String primaryKeyField = dtf.getPrimaryKeyField();
		sqlBuilder.append(COUNT_QUERY_BASIC).append(tableName).append(" ").append(WHERE).append(primaryKeyField)
				.append("='").append(key.toString()).append("'");
		return dao.getCountResult(sqlBuilder.toString(), tableName) > 0;
	}
}
