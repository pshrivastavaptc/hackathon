package com.ptc.hackathon.mbps.pojo;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.json.JSONObject;

import com.ptc.hackathon.Constants;

@Entity
@Table(name="DATA_TABLE_FIELDS")
public class DataTableFields {

	public String getDataTableName() {
		return dataTableName;
	}

	public void setDataTableName(String dataTableName) {
		this.dataTableName = dataTableName;
	}

	public String getDataShapeName() {
		return dataShapeName;
	}

	public void setDataShapeName(String dataShapeName) {
		this.dataShapeName = dataShapeName;
	}

	public String getFields() {
		return fields;
	}

	public void setFields(String fields) {
		this.fields = fields;
	}

	@Id
	@Column(name="DATA_TABLE_NAME")
	private String dataTableName;
	
	@Column(name="DATA_SHAPE_NAME")
	private String dataShapeName;
	
	@Column(name="FIELDS")
	private String fields;
	
	@Column(name = "PRIMARY_KEY_FIELD")
	private String primaryKeyField;
	
	public String getPrimaryKeyField() {
		return primaryKeyField;
	}

	public void setPrimaryKeyField(String primaryKeyField) {
		this.primaryKeyField = primaryKeyField;
	}

	public DataTableFields(String dataTableName, String dataShapeName, String fields, String primaryKeyField) {
		super();
		this.dataTableName = dataTableName;
		this.dataShapeName = dataShapeName;
		this.fields = fields;
		this.primaryKeyField = primaryKeyField;
	}

	public DataTableFields(){}
	
	public String typeOfField(String fieldName) throws Exception {
		int indexOfField = fields.indexOf(fieldName);
		String retVal = null;
		if(indexOfField != -1) {
			String subField = fields.substring(indexOfField);
			String[] fieldNameTypePair = subField.split(Constants.TYPE_NAME_DELIMITER);
			retVal = fieldNameTypePair[0].split(Constants.FIELD_DELIMITER)[1];
		}
		else
			throw new Exception("Field "+fieldName+" is not present");
		return retVal;
	}
}
