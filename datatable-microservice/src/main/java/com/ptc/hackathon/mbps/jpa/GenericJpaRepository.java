package com.ptc.hackathon.mbps.jpa;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;

import org.springframework.stereotype.Repository;

import com.ptc.hackathon.mbps.pojo.DataTableFields;
import com.ptc.hackathon.mbps.pojo.Employee;

@Repository
@Transactional
public class GenericJpaRepository {

	@PersistenceContext
	EntityManager entityManager;
	
	public Object update(Object dtf ) {
		return entityManager.merge(dtf);
	}

	/**
	 * method returns the DataTableFields of the table that is created. 
	 * @param id
	 * @return
	 */
	public DataTableFields findById(String id) {
		//TODO change the name of the method to mention its task clearly.
		return entityManager.find(DataTableFields.class, id);
	}
	
	public Employee findEmployeeById(Integer id) {
		return entityManager.find(Employee.class, id);
	}
	
	public Object insert (Object toInsert) {
		return entityManager.merge(toInsert);
	}
}
