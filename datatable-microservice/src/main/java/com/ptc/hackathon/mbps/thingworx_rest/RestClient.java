package com.ptc.hackathon.mbps.thingworx_rest;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptc.hackathon.mbps.pojo.ThingworxConfigBean;

@Service
public class RestClient {
	
	@Autowired
	private ThingworxConfigBean thingworxConfigBean;
	
	//private final String TWX_URL = thingworxConfigBean.getUrl();
	
	public String thingWorxGet(String path) throws Exception {
		try {
		URL url = new URL(thingworxConfigBean.getUrl()+"/"+path);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		String encoded = Base64.getEncoder().encodeToString(("Administrator"+":"+"trUf6yuz2?_Gub").getBytes(StandardCharsets.UTF_8));  //Java 8
		conn.setRequestProperty("Authorization", "Basic "+encoded);
		conn.setRequestMethod("GET");
		conn.setRequestProperty("Accept", "application/json");

		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : "
					+ conn.getResponseCode());
		}

		BufferedReader br = new BufferedReader(new InputStreamReader(
			(conn.getInputStream())));

		String output = br.readLine();
		conn.disconnect();
		return output;

	  } catch (MalformedURLException e) {
		e.printStackTrace();
	  } catch (IOException e) {
		e.printStackTrace();
	  }
		return null;
	}
}
