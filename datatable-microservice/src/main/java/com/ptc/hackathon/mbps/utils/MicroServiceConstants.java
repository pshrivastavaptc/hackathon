package com.ptc.hackathon.mbps.utils;

public class MicroServiceConstants {
	public static final String MATCHES = "MATCHES";
	public static final String NOTMATCHES = "NOTMATCHES";
	public static final String TAGGEDWITH = "TAGGEDWITH";
	public static final String NOTTAGGEDWITH = "NOTTAGGEDWITH";
	public static final String GT = "GT";
	public static final String LT = "LT";
	public static final String GE = "GE";
	public static final String LE = "LE";
	public static final String NE = "NE";
	public static final String EQ = "EQ";
	public static final String LIKE = "LIKE";
	public static final String NOTLIKE = "NOTLIKE";
	public static final String IN = "IN";
	public static final String NOTIN = "NOTIN";
	public static final String BETWEEN = "BETWEEN";
	public static final String NOTBETWEEN = "NOTBETWEEN";
	public static final String MISSINGVALUE = "MISSINGVALUE";
	public static final String NOTMISSINGVALUE = "NOTMISSINGVALUE";
	public static final String NEAR = "NEAR";
	public static final String NOTNEAR = "NOTNEAR";
	public static final String AND = "AND";
	public static final String OR = "OR";
}
