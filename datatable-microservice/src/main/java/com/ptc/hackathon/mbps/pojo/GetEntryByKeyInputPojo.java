package com.ptc.hackathon.mbps.pojo;

public class GetEntryByKeyInputPojo {

	private String tableName;
	
	private String key;

	public GetEntryByKeyInputPojo(String tableName, String key) {
		super();
		this.tableName = tableName;
		this.key = key;
	}

	public String getTableName() {
		return tableName;
	}

	public void setTableName(String tableName) {
		this.tableName = tableName;
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public GetEntryByKeyInputPojo() {
	}
	
	
}
