package com.ptc.hackathon.mbps.pojo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:TwxConf.properties")
public class ThingworxConfigBean {
	
	@Value("${Url}")
	private String url;
	
	@Value("${UserName}")
	private String userName;
	
	@Value("${Password}")
	private String password;

	public String getUserName() {
		return userName;
	}

	public String getPassword() {
		return password;
	}
	
	public String getUrl() {
		return url;
	}
}
