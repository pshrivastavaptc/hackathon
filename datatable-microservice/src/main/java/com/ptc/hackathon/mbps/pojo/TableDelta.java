package com.ptc.hackathon.mbps.pojo;

import java.util.ArrayList;
import java.util.List;

public class TableDelta {

	private List<TableColumn> addedColumns = new ArrayList<>();
	
	private List<TableColumn> removedColumns = new ArrayList<>();
	
	private List<TableColumn> modifiedColumns = new ArrayList<>();

	public List<TableColumn> getAddedColumns() {
		return addedColumns;
	}

	public void setAddedColumns(List<TableColumn> addedColumns) {
		this.addedColumns = addedColumns;
	}

	public List<TableColumn> getRemovedColumns() {
		return removedColumns;
	}

	public void setRemovedColumns(List<TableColumn> removedColumns) {
		this.removedColumns = removedColumns;
	}

	public List<TableColumn> getModifiedColumns() {
		return modifiedColumns;
	}

	public void setModifiedColumns(List<TableColumn> modifiedColumns) {
		this.modifiedColumns = modifiedColumns;
	}
	
	public void addRemoved(TableColumn column) {
		removedColumns.add(column);		
	}
	
	public void addAdded(TableColumn column) {
		addedColumns.add(column);		
	}
	
	public void addModified(TableColumn columnDelta){
		modifiedColumns.add(columnDelta);
	}
}
