package com.ptc.hackathon.mbps.apis;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.ptc.hackathon.mbps.dynamicpojogeneration.GeneratePojo;
import com.ptc.hackathon.mbps.dynamicpojogeneration.PojoGenerator;
import com.ptc.hackathon.mbps.jdbc.DataTableDao;
import com.ptc.hackathon.mbps.jpa.GenericJpaRepository;
import com.ptc.hackathon.mbps.pojo.CreateTableInputPojo;
import com.ptc.hackathon.mbps.pojo.DataTableFields;
import com.ptc.hackathon.mbps.pojo.GetEntryByKeyInputPojo;
import com.ptc.hackathon.mbps.pojo.QueryTableEntriesPojo;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.pojo.generated.GeneratedClass;
import com.ptc.hackathon.mbps.utils.DBUtils;
import com.ptc.hackathon.mbps.utils.Utilities;

import javassist.CtClass;

@RestController
public class DataTableInteractionAPI {

	@Autowired
	DataTableDao dataTableDao;

	@Autowired
	GenericJpaRepository jpaRepository;
	
	@Autowired
	DBUtils dbUtils;
	
	private static final Logger LOG = LoggerFactory.getLogger(DataTableInteractionAPI.class); 

	@GetMapping("/{dataTableName}/CountDataTableEntries")
	@ResponseBody
	public ResponseStructure CountDataTableEntries(@PathVariable("dataTableName") String dataTableName)
			throws Exception {
		ResponseStructure response = new ResponseStructure();
		Integer count = null;
		try {
			count = dataTableDao.countDataTableEntries(dataTableName);
			response.setResult(count);
			response.setStatus(200);
		} catch (Exception e) {
			response.setErrorMessage(e.getMessage());
			response.setStatus(500);
		}
		return response;
	}

	@PostMapping(value = "/{dataTableName}/AddDataTableEntry", consumes = MediaType.APPLICATION_JSON_VALUE)
	public void AddDataTableEntry(@PathVariable("dataTableName") String dataTableName,
			@RequestBody String reqJsonObject)
			throws Exception {

		/*JsonParser parser = new JsonParser();
		JsonElement ele = parser.parse(reqJsonObject);
		JsonObject jsonObject = ele.getAsJsonObject();
		*/
		JSONObject jsonObject = new JSONObject(reqJsonObject);
		
		DataTableFields dtf = jpaRepository.findById(dataTableName);
		Map<String, String> nameToTypeMap = Utilities.convertFieldsToMap(dtf);
		String className = PojoGenerator.GENERATED_CLASSES_PACKAGE + dataTableName;
		CtClass ct = PojoGenerator.generate(className, nameToTypeMap);

		if (ct != null) {
			Class<?> toClazz = null;
			try{
				toClazz = Class.forName(className);
			}catch (ClassNotFoundException e) {
				toClazz = ct.toClass(this.getClass().getClassLoader(), this.getClass().getProtectionDomain());
			}
			
			Object obj = toClazz.newInstance();
			//Session session = HibernateUtil.getSessionFactory().openSession();
			//session.beginTransaction();
			

			Iterator<?> keys = jsonObject.keys();

			ArrayList getters = new ArrayList<String>();
			while (keys.hasNext()) {
				String key = (String) keys.next();
				String fieldName = key.substring(0, 1).toUpperCase();
				String setterName = "set" + fieldName + key.substring(1);
				String getterName = "get"+fieldName+key.substring(1);
				Method getter = toClazz.getMethod("get"+fieldName+key.substring(1));
				//getter.invoke(obj, jsonObject.get(key));
				Object value = jsonObject.get(key);
				if(getter.getReturnType().equals(Date.class)) {
					Long dateLong = (Long)value;
					value = new Date(dateLong);
				}
				toClazz.getMethod(setterName, getter.getReturnType()).invoke(obj, value);
				
				// Need to remove the below logic with something better
				//String getterName = "get" + key.substring(0, 1).toUpperCase() + key.substring(1);
				getters.add(getterName);
			}
			
			StringBuilder sql = dbUtils.addDataTableEntriesSql(dataTableName, nameToTypeMap);
			
			dataTableDao.addDataTableEntries(sql, obj, nameToTypeMap);
		}
	}

	@PostMapping("/CreateDataTable")
	public ResponseStructure createDataTable(@RequestParam("tableName") String tableName,
			@RequestParam("dataShapeName") String dataShapeName) throws Exception {
		ResponseStructure response = new ResponseStructure();

/*		JSONObject dataShapeJson = new JSONObject(dbUtils.obtainDataShapeJson(dataShapeName));
		
		TableColumn[] columns = dbUtils.obtainColumnsFromDataShape(dataShapeJson);
		Table newTable = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(newTable);
		dataTableDao.executeSql(sql);
		dbUtils.commitDataTableFields(tableName, dataShapeName, columns);*/
		response.setErrorMessage("Removed!!! Use 'CreateDataTableJson' instead");
		response.setResult(null);
		response.setStatus(200);
		return response;
	}

	@PostMapping(value = "/CreateDataTableJson", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseStructure createDataTableJson(@RequestBody CreateTableInputPojo createTableInput) throws Exception {
		String tableName = createTableInput.getTableName();
		String dataShapeName = createTableInput.getDataShapeName();
		ResponseStructure response = new ResponseStructure();		
		JSONObject dataShapeJson = new JSONObject(dbUtils.obtainDataShapeJson(dataShapeName));
		TableColumn[] columns = dbUtils.obtainColumnsFromDataShape(dataShapeJson);
		Table newTable = new Table(tableName, columns);
		dbUtils.addDefaultColumnsToTable(newTable);
		columns = newTable.getColumns();
		// If table already exists, that means that the schema has been updated the handle update
		boolean tableExists = dbUtils.checkTableExists(tableName.toUpperCase());
		if(tableExists){
			dbUtils.handleTableUpdate(newTable);
		}
		else {

			// Else create table
			String sql = dbUtils.createDataTableSql(newTable);
			dataTableDao.executeSql(sql);

			DataTableFields dtf = dbUtils.commitDataTableFields(tableName, dataShapeName, columns);
			Map<String, String> nameToTypeMap = Utilities.convertFieldsToMap(dtf);
			PojoGenerator.generate(PojoGenerator.GENERATED_CLASSES_PACKAGE + tableName, nameToTypeMap).toClass();
		}
		return response;
	}

	@PostMapping(value = "/QueryDataTableEntries", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GeneratedClass> queryDataTableEntries(@RequestBody  QueryTableEntriesPojo queryTableEntries) throws Exception {
		//ResponseStructure response = new ResponseStructure();
		String dataTableName = queryTableEntries.getTableName();
		String queryString = queryTableEntries.getQuery();
		JSONObject query = new JSONObject(queryString);
		String sql = dbUtils.obtainParsedQuerySQL(query, dataTableName);
		LOG.debug("QueryDataTableEntries API called to execute SQL {}", sql);
		return dbUtils.findEntries(sql, dataTableName);
	}
	
	@PostMapping(value = "/GetDataTableEntryByKey", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public List<GeneratedClass> getDataTableEntryByKey(@RequestBody  GetEntryByKeyInputPojo getEntryByKeyInput) throws Exception {
		//ResponseStructure response = new ResponseStructure();
		String dataTableName = getEntryByKeyInput.getTableName();
		Object key = getEntryByKeyInput.getKey();
		//Class dataTableClass = Class.forName(PojoGenerator.GENERATED_CLASSES_PACKAGE+dataTableName);
		List<GeneratedClass> retVal =  new ArrayList<GeneratedClass>();
		retVal.add(dbUtils.findById(dataTableName, key));
		return retVal;
	}
	
	@PostMapping(value="/RemoveDataTable", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseStructure removeDataTable(@RequestBody String removeTableInput) throws JSONException {
		ResponseStructure rs = new ResponseStructure();
		JSONObject json = new JSONObject(removeTableInput);
		String dataTableName = json.getString("dataTableName");
		try{
			dbUtils.removeDataTable(dataTableName);
			rs.setResult(null);
			rs.setErrorMessage(null);
			rs.setStatus(200);
		} catch(Exception e) {
			rs.setErrorMessage(e.getMessage());
			rs.setStatus(500);
			rs.setResult(null);
		}
		return rs;
	}
	
	/**
	 * To update the entry the key is a mandatory field.
	 * @param dataTableName
	 * @param values
	 * @return
	 * @throws Exception
	 */
	@PostMapping(value="{dataTableName}/UpdateDataTableEntries", consumes = MediaType.APPLICATION_JSON_VALUE)
	public ResponseStructure updateDataTableEntries(@PathVariable("dataTableName") String dataTableName, @RequestBody String values) throws Exception {
		ResponseStructure rs = new ResponseStructure();
		JSONObject updatedObject = new JSONObject(values);
		dbUtils.updateEntry(dataTableName, updatedObject);
		rs.setStatus(200);
		return rs;
	}
	
	@PostMapping(value="{dataTableName}/KeyExists", consumes = MediaType.APPLICATION_JSON_VALUE)
	public Boolean keyExists(@PathVariable("dataTableName") String dataTableName, @RequestBody String jsonKey) throws Exception {
		JSONObject keyObject = new JSONObject(jsonKey);
		return dbUtils.checkKeyExists(dataTableName, keyObject.getString("primaryKey"));
	}
}
