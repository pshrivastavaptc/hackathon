package com.ptc.hackathon.mbps.pojo;

public class Table {

	private String tableName;
	private TableColumn[] columns;
	

	public Table(String tableName, TableColumn[] columns) {
		this.tableName = tableName;
		this.columns = columns;
	}
	public Table() {
		// TODO Auto-generated constructor stub
	}
	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public TableColumn[] getColumns() {
		return columns;
	}
	public void setColumns(TableColumn[] columns) {
		this.columns = columns;
	}
}
