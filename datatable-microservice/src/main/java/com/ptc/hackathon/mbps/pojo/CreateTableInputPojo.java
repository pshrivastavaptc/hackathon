package com.ptc.hackathon.mbps.pojo;

public class CreateTableInputPojo {

	public String getTableName() {
		return tableName;
	}
	public void setTableName(String tableName) {
		this.tableName = tableName;
	}
	public String getDataShapeName() {
		return dataShapeName;
	}
	public void setDataShapeName(String dataShapeName) {
		this.dataShapeName = dataShapeName;
	}
	public CreateTableInputPojo(String tableName, String dataShapeName) {
		super();
		this.tableName = tableName;
		this.dataShapeName = dataShapeName;
	}
	public CreateTableInputPojo () {}
	private String tableName;
	private String dataShapeName;
}
