package com.ptc.hackathon.mbps.utils;

import java.util.HashMap;
import java.util.Map;

import com.ptc.hackathon.Constants;
import com.ptc.hackathon.mbps.pojo.DataTableFields;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.pojo.TableDelta;

public class Utilities {

	public static Map<String, String> convertFieldsToMap(DataTableFields dtf){
		Map<String, String> retVal = new HashMap<String, String>();
		String fields = dtf.getFields();
		String[] nameTypePairs = fields.split(Constants.FIELD_DELIMITER);
		for(String nameType : nameTypePairs) {
			String[] nameAndType = nameType.split(Constants.TYPE_NAME_DELIMITER);
			retVal.put(nameAndType[0], nameAndType[1]);
		}
		return retVal;
	}
	
	
	public static TableDelta calculateTableDelta(Table oldTable, Table newTable) {
		TableDelta delta = new TableDelta();
		
		for(TableColumn newColumn : newTable.getColumns()) {
			if(!tableHasColumnByName(oldTable, newColumn)){
				delta.addAdded(newColumn);
			}
			else if(tableHasColumn(oldTable, newColumn)){
				delta.addModified(newColumn);
			}
		} 
		
		for(TableColumn oldColumn : oldTable.getColumns()) {
			if(!tableHasColumnByName(newTable, oldColumn)){
				delta.addRemoved(oldColumn);
			}
		}
		
		//TODO calculate modified columns
		
		return delta;
	}
	
	public static boolean tableHasColumn(Table table, TableColumn column) {
		for(TableColumn tableColumn: table.getColumns()) {
			if(tableColumn.equals(column)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean tableHasColumnByName(Table table, TableColumn column) {
		for(TableColumn tableColumn: table.getColumns()) {
			if(tableColumn.getColumnName().equalsIgnoreCase(column.getColumnName())) {
				return true;
			}
		}
		return false;
	}
}
