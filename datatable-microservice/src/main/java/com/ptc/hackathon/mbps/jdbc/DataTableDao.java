package com.ptc.hackathon.mbps.jdbc;

import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

//import org.h2.jdbc.JdbcSQLException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.ptc.hackathon.GeneratedPojoRowMapper;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.pojo.generated.GeneratedClass;
import com.ptc.hackathon.mbps.utils.DBUtils;

@Repository
public class DataTableDao {

	@Autowired
	public JdbcTemplate jdbcTemplate;
	
	public Integer countDataTableEntries(String tableName) throws Exception {
		String sqlQuery = DBUtils.COUNT_QUERY_BASIC+tableName;
		return getCountResult(sqlQuery, tableName);
	}
	
	public void createDataTable(String tableName, JSONObject structure) {
		
	}
	
	public void executeSql(String sql) {
		jdbcTemplate.execute(sql);
	}

	public JdbcTemplate getJdbcTemplate() {
		return jdbcTemplate;
	}
	
	public String getTableNameForSchema() {
		 return "INFORMATION_SCHEMA.TABLES";
	}
	
	public String getColumnForTableNameSchema() {
		return "TABLE_NAME";
	}
	
	public Integer getCountResult(String sqlQuery, String tableName) throws Exception {
		Integer retVal = null;
		try{
			retVal = jdbcTemplate.queryForObject(sqlQuery, Integer.class);
		} catch (Exception e) {
			/*if(e.getCause() instanceof JdbcSQLException) {
				JdbcSQLException sqlException = (JdbcSQLException)e.getCause();
				String errorMessage = sqlException.getOriginalMessage();
				if(errorMessage.equalsIgnoreCase("Table \""+tableName+"\" not found")) {
					throw new Exception(errorMessage);
				}*/
			//}
		}
		return retVal;
	}
	
	
	public <T> T findById(String sql, Class resultType) throws Exception {	
		
		//jdbcTemplate.queryForObject(sql, new BeanPropertyRowMapper<resultType.getClass()>(resultType));
		T retVal = (T) jdbcTemplate.queryForObject(sql, new GeneratedPojoRowMapper(resultType));
		return retVal;
	}


	public void addDataTableEntries(StringBuilder sql, Object obj, Map<String, String> nameToTypeMap) throws Exception {
		List<Object> getterResults = new ArrayList<Object>();
		
		
		
		for(String str : nameToTypeMap.keySet()) {
			
			
			String fieldName = str.substring(0, 1).toUpperCase();
			String getterName = "get"+fieldName+str.substring(1);
			
			try {
				Method method = obj.getClass().getMethod(getterName);
				getterResults.add(method.invoke(obj));
			}
			catch(Exception e) {
				getterResults.addAll(null);
			}
			
		}
		
		// default values should be null
		// map the value to the right parameter
		
		jdbcTemplate.update(sql.toString(), getterResults.toArray(new Object[getterResults.size()]));

	}
	
	public List<GeneratedClass> query(String sql, Class resultType) {
		return jdbcTemplate.query(sql, new GeneratedPojoRowMapper(resultType));
	}
	
	
	public List<TableColumn> obtainTableColumnsFromDB(String sql, final String tableName) {
		return  jdbcTemplate.query(sql, new RowMapper<TableColumn> () {

			@Override
			public TableColumn mapRow(ResultSet rs, int rowNum) throws SQLException {
				TableColumn retVal = null;
				String columnName = rs.getString("FIELD");
				String type = "STRING";
				String key = rs.getString("KEY");
				boolean isPrimary = key.equals("PRI");
				retVal = new TableColumn(columnName, type, isPrimary);
				return retVal;
			}
			
		});
	}
	
	public List<Map<String, Object>> getDataTable() {
		String sql = "select distinct entity_id, field_values from data_table";
		List<Map<String, Object>> list = jdbcTemplate.queryForList(sql);
		
		//DataTable dataTableObj = (DataTable) jdbcTemplate.queryForObject(sql,new Object[] { tableMap }, new DataTableRowMapper());
		return list;
	}
	
	public String executeSQLForString(String sql) {
		return (String)(jdbcTemplate.queryForObject(sql, String.class));
	}
	
}
