package com.ptc.hackathon.mbps.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.hibernate.SessionFactory;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Environment;

import com.github.fluent.hibernate.cfg.scanner.EntityScanner;


public class HibernateUtil2 {

	private static SessionFactory sessionFactory;
	   private static StandardServiceRegistry registry;

/*	private static SessionFactory buildSessionFactory() {
		try {
			// Create the SessionFactory from hibernate.cfg.xml
			return new Configuration().configure().buildSessionFactory();
		} catch (Throwable ex) {
			// Make sure you log the exception, as it might be swallowed
			System.err.println("Initial SessionFactory creation failed." + ex);
			throw new ExceptionInInitializerError(ex);
		}
	}*/
	
	public static SessionFactory getSessionFactory() {
	      if (sessionFactory == null) {
	         try {

	            // Create registry builder
	            StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();

	            // Hibernate settings equivalent to hibernate.cfg.xml's properties
	            Map<String, String> settings = new HashMap<>();
	            settings.put(Environment.DRIVER, "org.h2.Driver");
	            settings.put(Environment.URL, "jdbc:h2:file:~/test");
	            settings.put(Environment.USER, "sa");
	            settings.put(Environment.PASS, "");
	            settings.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
	            settings.put(Environment.HBM2DDL_AUTO, "create-drop");
	            // Apply settings
	            registryBuilder.applySettings(settings);
	            
	            List<Class<?>> classes = EntityScanner
	                    .scanPackages("com.ptc.hackathon.generatedclasses").result();

	            // Create registry
	            registry = registryBuilder.build();

	            // Create MetadataSources
	            MetadataSources sources = new MetadataSources(registry);

	            for (Class<?> annotatedClass : classes) {
	                sources.addAnnotatedClass(annotatedClass);
	            }
	            
	            // Create Metadata
	            Metadata metadata = sources.getMetadataBuilder().build();

	            // Create SessionFactory
	            sessionFactory =  metadata.getSessionFactoryBuilder().build();

	         } catch (Exception e) {
	            e.printStackTrace();
	            if (registry != null) {
	               StandardServiceRegistryBuilder.destroy(registry);
	            }
	         }
	      }
	      return sessionFactory;
	   }
	

	public static void shutdown() {
		// Close caches and connection pools
		getSessionFactory().close();
	}

}