package com.ptc.hackathon.datamigration;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ptc.hackathon.mbps.jdbc.DataTableDao;

@Service
public class GenerateSql {

	@Autowired
	DataTableDao dataTableDao;

	public void process() throws JSONException {
		List<Map<String, Object>> list = getDataTables();
		Map<String, JSONObject> tablesToCreate = new HashMap();
		// get the name of coloumns and process the json object
		for (Map<String, Object> map : list) {
			for (String key : map.keySet()) {
				String keyToInsert = (String) map.get("ENTITY_ID");
				String valueToInsert = (String) map.get("FIELD_VALUES");
				JSONObject obj = new JSONObject(valueToInsert);
				tablesToCreate.put(keyToInsert, obj);
/*
				StringBuilder sql = generateInsertSql(keyToInsert, obj);
				if (sql != null)
					writeFile(sql, false);*/
			}
		}

		for (String key : tablesToCreate.keySet()) {
			JSONObject obj = tablesToCreate.get(key);
			StringBuilder sql = generateCreateTableSql(key, obj);
			if (sql != null)
				writeFile(sql, false);
		}
		
		for (Map<String, Object> map : list) {
			
				String keyToInsert = (String) map.get("ENTITY_ID");
				String valueToInsert = (String) map.get("FIELD_VALUES");
				JSONObject obj = new JSONObject(valueToInsert);
				tablesToCreate.put(keyToInsert, obj);

				StringBuilder sql = generateInsertSql(keyToInsert, obj);
				if (sql != null)
					writeFile(sql, false);
			
		}
		
		

		System.out.println("Checking");
	}

	private StringBuilder generateInsertSql(String tableName, JSONObject columns) throws JSONException {
		StringBuilder sql = new StringBuilder("INSERT INTO ");
		sql.append(tableName);
		sql.append(" ( ");

		StringBuilder col = new StringBuilder();
		StringBuilder values = new StringBuilder();
		JSONArray keys = columns.names();

		for (int i = 0; i < keys.length(); ++i) {
			String key = keys.getString(i);
			String val = columns.getString(key);

			col.append(key);
			col.append(", ");
			values.append("'");
			values.append(val);
			values.append("'");
			values.append(", ");
		}

		String str = col.substring(0, col.length() - 1);
		sql.append(str);
		sql.append(" ) values ( ");
		str = values.substring(0, values.length() - 1);
		sql.append(str);
		sql.append(" );");

		return sql;
	}

	private StringBuilder generateCreateTableSql(String tableName, JSONObject columns) throws JSONException {
		StringBuilder sql = new StringBuilder("CREATE TABLE ");
		sql.append(tableName);
		sql.append("(");
		StringBuilder col = new StringBuilder();

		JSONArray keys = columns.names();

		for (int i = 0; i < keys.length(); ++i) {

			String key = keys.getString(i);
			col.append(key);
			col.append(" ");
			col.append("varchar(255)");
			col.append(",");

		}
		String str = col.substring(0, col.length() - 1);
		col = new StringBuilder(str).append(")");
		sql.append(col);
		sql.append(";");
		return sql;
	}

	public void writeFile(StringBuilder sql, boolean writeAtBegining) {

		BufferedWriter bw = null;
		FileWriter fw = null;
		RandomAccessFile writer = null;

		try {
			if (writeAtBegining) {

			} else {
				File file = new File("SomeSQLFile.sql") ;
				if(!file.exists()) {
					file.createNewFile();
				}
				fw = new FileWriter(file, true);
				bw = new BufferedWriter(fw);
				bw.write(sql.toString());
				bw.write("\r\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {

			try {
				if (writeAtBegining) {
					//writer.close();
				} else {
					if (bw != null)
						bw.close();

					if (fw != null)
						fw.close();
				}

			} catch (IOException ex) {

				ex.printStackTrace();
			}
		}
	}

	public List<Map<String, Object>> getDataTables() {
		List<Map<String, Object>> nameAndCol = dataTableDao.getDataTable();
		return nameAndCol;
	}

}
