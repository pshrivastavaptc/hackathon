package com.ptc.hackathon;

public class Constants {

	public static final String FIELD_DELIMITER = ";";
	public static final String TYPE_NAME_DELIMITER = ":";
}
