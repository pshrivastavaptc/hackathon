package com.ptc.hackathon;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

import com.ptc.hackathon.mbps.pojo.generated.GeneratedClass;

public class GeneratedPojoRowMapper implements RowMapper<GeneratedClass>{

	private Class type;
	
	public GeneratedPojoRowMapper(Class type) {
		this.type = type;
	}
	
	@Override
	public GeneratedClass mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		try {
			GeneratedClass retVal = (GeneratedClass)type.newInstance();

			for(Method method : type.getDeclaredMethods()) {
				if(method.getName().startsWith("get")){
				String field = method.getName().substring(3);
				Method setterMethod = type.getMethod("set"+field, method.getReturnType());
				setterMethod.invoke(retVal, rs.getObject(field));
				}
			}
		return retVal;	
		} catch (Exception e) {
			e.printStackTrace();
		} 
		
		return null;
	}

}
