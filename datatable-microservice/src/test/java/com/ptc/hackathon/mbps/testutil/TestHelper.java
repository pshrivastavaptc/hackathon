package com.ptc.hackathon.mbps.testutil;

import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import com.ptc.hackathon.DatatableMicroserviceApplication;

public class TestHelper {

	private static ConfigurableApplicationContext appContext;

	public static void startApp() throws Exception {
		System.out.println("######### Starting Database MicroService ############");
		SpringApplication app = new SpringApplication(DatatableMicroserviceApplication.class);
		appContext = app.run(new String[] {});
		System.out.println("######### Database MicroService started ############");
	}

	public static void stopApp() {
		System.out.println("######### Stopping Database MicroService ############");
		appContext.stop();
		appContext = null;
		System.out.println("######### Database MicroService Stopped ############");
	}
	
}
