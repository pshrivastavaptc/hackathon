package com.ptc.hackathon.mbps.utils;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptc.hackathon.DatatableMicroserviceApplication;
import com.ptc.hackathon.mbps.jdbc.DataTableDao;
import com.ptc.hackathon.mbps.jpa.GenericJpaRepository;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes ={DatatableMicroserviceApplication.class})
public class DBUtilsTest {

	@Autowired
	DBUtils dbUtils;
	
	@Autowired
	DataTableDao dao;
	
	@Autowired
	GenericJpaRepository jpaRepository;
	
	private static final String tableName = "student";
	
	@Before
	public void cleanTableBeforeTests() {
		cleanUpStudent();
	}
	
	@Test
	public void test_AddDefaultColumns() {
		TableColumn[] columns = new TableColumn[1];
		columns[0]  = new TableColumn("field1","STRING",true);
		Table table = new Table("testTable", columns);
		dbUtils.addDefaultColumnsToTable(table);
		int columnsSize = table.getColumns().length;
		assertEquals(7,columnsSize);
	}
	
	@Test
	public void test_obtainTableFromDB() {
		//String tableName = "Student";
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		Table studentTable = dbUtils.obtainCurrentTable(tableName);
		
		for(TableColumn column : studentTable.getColumns()){
			System.out.println(column.getColumnName());
		}
		System.out.println();
		cleanUpStudent();
		assertEquals(2, studentTable.getColumns().length);
	}
	
	@Test
	public void test_AddColumnOnTable() {
		//String tableName = "Student1";
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		TableColumn[] newColumns = new TableColumn[3];
		newColumns[0] = new TableColumn("RollNumber","INTEGER",true);
		newColumns[1] = new TableColumn("Name","STRING",false);
		newColumns[2] = new TableColumn("FatherName","STRING",false);
		
		Table modifiedTable = new Table(tableName, newColumns);
		
		dbUtils.handleTableUpdate(modifiedTable);
		
		
		Table studentTable = dbUtils.obtainCurrentTable(tableName);
		for(TableColumn column : studentTable.getColumns()){
			System.out.println(column.getColumnName());
		}
		cleanUpStudent();
		assertEquals(3, studentTable.getColumns().length);
	}
	
	@Test
	public void test_RemoveColumnOnTable() {
		//String tableName = "Student2";
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		TableColumn[] newColumns = new TableColumn[1];
		newColumns[0] = new TableColumn("RollNumber","INTEGER",true);
		
		Table modifiedTable = new Table(tableName, newColumns);
		
		dbUtils.handleTableUpdate(modifiedTable);
		
		
		Table studentTable = dbUtils.obtainCurrentTable(tableName);
		for(TableColumn column : studentTable.getColumns()){
			System.out.println(column.getColumnName());
		}
		cleanUpStudent();
		assertEquals(1, studentTable.getColumns().length);
	}
	
	@Test
	public void test_CheckTableExists () throws Exception {
		//String tableName = "Student3";
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		
		boolean exists = dbUtils.checkTableExists(tableName);
		cleanUpStudent();
		assertTrue(exists);	
		
	}
	
	@Test
	public void test_ModifyColumnOnTable() {
		//String tableName = "Student4";
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		String insertQuery = "insert into student values (1234,'asdf')";
		dao.executeSql(insertQuery);
		
		
		TableColumn[] newColumns = new TableColumn[2];
		newColumns[0] = new TableColumn("RollNumber","STRING",true);
		newColumns[1] = new TableColumn("Name","STRING",false);
		
		Table modifiedTable = new Table(tableName, newColumns);
		
		dbUtils.handleTableUpdate(modifiedTable);
		
		insertQuery = "insert into student values ('qwer','zxcv')";
		dao.executeSql(insertQuery);
		cleanUpStudent();
		assertEquals(2, 2);
	}
	
	private void cleanUpStudent() {
		try {
		String sql = "DROP TABLE STUDENT";
		dao.executeSql(sql);
		} catch(Exception e) {
			
		}
	}

	@Test
	public void test_UpdateEntry() throws Exception {
		TableColumn[] columns = new TableColumn[3];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		columns[2] = new TableColumn("key","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		String insertQuery = "insert into student values (1234,'asdf','qwer-qwer-qwer-qwer')";
		dao.executeSql(insertQuery);
		dbUtils.commitDataTableFields(tableName, "testDataShape", columns);
		String updateString = "{'RollNumber':1234,'Name':'razzak','key':'abcd-abcd-abcd-abcd'}";
		JSONObject updatedObject = new JSONObject(updateString);
		System.out.println(updatedObject);
		String updateQuery = dbUtils.updateEntrySql(tableName, updatedObject);
		System.out.println(updateQuery);
		
		dao.executeSql(updateQuery);
		if(dao.jdbcTemplate == null) {
			System.out.println("template is null");
		}
		String result_Name = dao.executeSQLForString("select name from "+tableName+" where RollNumber='1234'");
		String result_Key = dao.executeSQLForString("select key from "+tableName+" where RollNumber='1234'");
		
		assertEquals("razzak",result_Name);
		assertEquals("abcd-abcd-abcd-abcd",result_Key);
		cleanUpStudent();
	}
	
	@Test
	public void test_checkKeyExists() throws Exception {
		TableColumn[] columns = new TableColumn[3];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		columns[2] = new TableColumn("key","STRING",false);
		
		Table table = new Table(tableName, columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		dbUtils.commitDataTableFields(tableName, "testDataShape", columns);
		String insertQuery = "insert into student values (1234,'asdf','qwer-qwer-qwer-qwer')";
		dao.executeSql(insertQuery);
		
		boolean result = dbUtils.checkKeyExists(tableName, "1234");
		assertTrue(result);
	}
}
