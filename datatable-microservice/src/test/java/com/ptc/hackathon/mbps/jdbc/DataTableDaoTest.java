package com.ptc.hackathon.mbps.jdbc;

import java.util.HashMap;
import java.util.Map;

import javax.validation.constraints.AssertTrue;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptc.hackathon.DatatableMicroserviceApplication;
import com.ptc.hackathon.mbps.dynamicpojogeneration.PojoGenerator;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.utils.DBUtils;
import static org.junit.Assert.*;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatatableMicroserviceApplication.class})
public class DataTableDaoTest {

	@Autowired
	DataTableDao dao;
	
	@Autowired
	DBUtils dbUtils;
	
	@Test
	public void test_selectQueries() throws Exception {
		TableColumn[] columns = new TableColumn[2];
		columns[0] = new TableColumn("RollNumber","INTEGER",true);
		columns[1] = new TableColumn("Name","STRING",false);
		
		Table table = new Table("Student", columns);
		String sql = dbUtils.createDataTableSql(table);
		dao.executeSql(sql);
		
		String insertQuery = "Insert into Student Values (1,'Saksham')";
		dao.executeSql(insertQuery);
		
		insertQuery = "Insert into Student Values (2,'Pragam')";
		dao.executeSql(insertQuery);
		
		Map<String,String> properties = new HashMap<String,String>();
		properties.put("rollNumber", "INTEGER");
		properties.put("name", "STRING");
		Class generatedClass = PojoGenerator.generate("com.ptc.hackathon.mbps.pojo.Student", properties).toClass();
		
		//Object obj = generatedClass.newInstance();
		
		String selectQuery = "Select * from Student where RollNumber=1";
		
		Object obj = dao.findById(selectQuery, generatedClass);
		System.out.println(obj);
		assertTrue(generatedClass.isInstance(obj) );
		
	}

}
