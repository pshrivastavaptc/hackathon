package com.ptc.hackathon.mbps.dynamicpojogeneration;

import static org.junit.Assert.*;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import org.junit.Test;

import javassist.CtClass;

public class PojoGeneratorTest {

	@Test
	public void test_ClassGeneration() throws Exception {
		Map<String,String> props = new HashMap<String,String>();
		props.put("employeeId", "INTEGER");
		props.put("firstName", "STRING");
		props.put("lastName", "STRING");
		
		String className = "com.ptc.generated.test.EmployeeTest";
		
		CtClass cc = PojoGenerator.generate(className, props);
		cc.toClass();		
		Class generatedClass = Class.forName(className);
		for(Method method : generatedClass.getMethods())
		{
			System.out.println(method.getReturnType() + " "+ method.getName()+" ("+method.getParameters()+")");
		}
		assertEquals(15, generatedClass.getMethods().length);	
	}
	
	public void test_addAnnotations() throws Exception {
		Map<String,String> props = new HashMap<String,String>();
		props.put("employeeId", "INTEGER");
		props.put("firstName", "STRING");
		props.put("lastName", "STRING");
		
		String className = "com.ptc.generated.test.EmployeeTest";
		
	}

}
