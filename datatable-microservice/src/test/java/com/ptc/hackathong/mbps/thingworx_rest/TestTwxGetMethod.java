package com.ptc.hackathong.mbps.thingworx_rest;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.ptc.hackathon.DatatableMicroserviceApplication;
import com.ptc.hackathon.mbps.pojo.Table;
import com.ptc.hackathon.mbps.pojo.TableColumn;
import com.ptc.hackathon.mbps.thingworx_rest.RestClient;
import com.ptc.hackathon.mbps.utils.DBUtils;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {DatatableMicroserviceApplication.class})
public class TestTwxGetMethod {

	@Autowired
	RestClient rc;
	
	@Autowired
	DBUtils dbUtils;
	
	@Test
	public void test_getDataShape() throws Exception {
		//rc.thingWorxGet("DataShapes/datashape_employee/FieldDefinitions");
	}
	
	@Test
	public void test_CreateTableSQL() {
		TableColumn[] column = new TableColumn[1];
		column[0] = new TableColumn("ID","STRING",true);
		Table table = new Table("testTable", column);
		dbUtils.createDataTableSql(table);
	}

}
