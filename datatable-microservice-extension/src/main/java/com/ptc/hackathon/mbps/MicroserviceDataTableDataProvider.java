
package com.ptc.hackathon.mbps;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.UUID;

import org.joda.time.DateTime;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.ptc.hackathon.mbps.util.RestClientAPIRepository;
import com.thingworx.datatables.DataTableEntry;
import com.thingworx.datatables.DataTableThing;
import com.thingworx.entities.utils.ThingUtilities;
import com.thingworx.metadata.DataShapeDefinition;
import com.thingworx.persistence.common.ExecutionContext;
import com.thingworx.persistence.provider.DataTableQueryCriteria;
import com.thingworx.persistence.provider.IDataEntryCloseableIterator;
import com.thingworx.persistence.provider.IDataTableDataProvider;
import com.thingworx.persistence.provider.SearchProviderQueryCriteria;
import com.thingworx.persistence.search.IDataElement;
import com.thingworx.types.TagCollection;
import com.thingworx.types.collections.ValueCollection;
import com.thingworx.types.data.queries.Query;
import com.thingworx.types.primitives.structs.Location;

public class MicroserviceDataTableDataProvider implements IDataTableDataProvider {

    private static MicroserviceDataTableDataProvider _dataProvider = null;
    
    private List<String> addedKeys = new ArrayList<String>();

    private MicroserviceDataTableDataProvider() {
    }

    public static MicroserviceDataTableDataProvider getInstance() {
        if (_dataProvider == null) {
            synchronized (MicroserviceDataTableDataProvider.class) {
                if (_dataProvider == null) {
                    _dataProvider = new MicroserviceDataTableDataProvider();
                }
            }
        }
        return _dataProvider;
    }

    @Override
    public LinkedHashSet<IDataElement> queryEntries(SearchProviderQueryCriteria arg0, ExecutionContext arg1) throws Exception {
        return null;
    }

    @Override
    public String addEntry(DataTableEntry dataTableEntry, ExecutionContext arg1) throws Exception {
        // System.out.println(arg0.getValues().toJSON().toString());
        JSONObject dataEntryTop = dataTableEntry.toJSON();
        JSONObject dataEntryValues = dataTableEntry.getValues().toJSON();dataTableEntry.createKeyFromValues(arg1.getDataShape());
        String dataEntryString = getMergedDataEntry(dataEntryTop, dataEntryValues);
        try {
            RestClientAPIRepository.getInstance().addTableEntry(arg1.getEntityName(), dataEntryString);
        } catch (Exception e) {
            return null;
        }
        return null;
    }

    private String getMergedDataEntry(JSONObject dataEntryTop, JSONObject dataEntryValues) throws Exception {
        Iterator<String> itDatEntrytop = dataEntryTop.keys();
        Iterator<String> itDataEntryValues = dataEntryValues.keys();
        JSONObject entry = new JSONObject();
        while (itDatEntrytop.hasNext()) {
            String key = itDatEntrytop.next();
            if (key == "tags") {
                entry.put(key, replaceTags(dataEntryTop.optJSONArray(key)));
            } 
            else if(key == "containerID") {
            	String generatedKey = UUID.randomUUID().toString();
            	addedKeys.add(generatedKey);
                entry.put("key", generatedKey);
                continue;
            }
            else {
                entry.put(key, dataEntryTop.get(key));
            }
        }
        while (itDataEntryValues.hasNext()) {
            String key = itDataEntryValues.next();
            entry.put(key, dataEntryValues.get(key));

        }
        return entry.toString();
    }

    private String replaceTags(JSONArray tag) throws Exception {
        TagCollection tagCol = TagCollection.fromJSON(tag);
        return tagCol.toString();
    }

    @Override
    public void deleteEntry(DataTableEntry arg0, ExecutionContext arg1) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void dispose(ExecutionContext arg0) throws Exception {
        try {
            RestClientAPIRepository.getInstance().removeDataTable(arg0.getEntityName());
        } catch (Exception e) {
            throw new Exception("MicroService is not in running state");
        }

    }

    @Override
    public List<DataTableEntry> getEntries(int arg0, ExecutionContext arg1) throws Exception {
        List<DataTableEntry> listDataEntry = new ArrayList<>();
        JSONArray resultSet = null;
        String stringQuery = "{}";
        try {
            resultSet =
                RestClientAPIRepository.getInstance().queryTableEntries(arg1.getEntityName(), stringQuery);
            if (resultSet == null)
                throw new Exception("No result from client side");
        } catch (Exception e) {
            return listDataEntry;
        }
        ArrayList<String> fieldNames = arg1.getDataShape().getFields().getNames();
        DataShapeDefinition dsf = arg1.getDataShape();
        listDataEntry = generateDataTableEntries(resultSet, fieldNames, dsf);
        return listDataEntry;
    }

    @Override
    public DataTableEntry getEntryById(String arg0, ExecutionContext arg1) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean idExists(String arg0, ExecutionContext arg1) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public void initializeDataProvider(ExecutionContext arg0) throws Exception {
        try {
        	DataTableThing dataTableThing = (DataTableThing)ThingUtilities.findThing(arg0.getEntityName());
        	String dataShapeName = dataTableThing.getDataShapeName();
        	 RestClientAPIRepository.getInstance().createTable(arg0.getEntityName(), dataShapeName);
        	
        } catch (Exception e) {
        }
    }

	@Override
    public void purgeEntries(ExecutionContext arg0) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public void reindex(ExecutionContext arg0) throws Exception {
        // TODO Auto-generated method stub

    }

    @Override
    public String updateEntry(DataTableEntry entry, ExecutionContext arg1) throws Exception {
        JSONObject updateEntry = covertEntryToString(entry.getValues().toJSON());
    	try {
            RestClientAPIRepository.getInstance().updateTableEntry(arg1.getEntityName(),updateEntry);
        } catch (Exception e) {
            throw new Exception("Unable to update query");
        }
        return null;
    }

    private JSONObject covertEntryToString(JSONObject json) throws Exception {
		JSONObject job = new JSONObject();
		Iterator<String> it = json.keys();
		while(it.hasNext()) {
			String key = it.next();
			job.put(key, json.opt(key).toString());
		}
		return job;
	}

	@Override
    public int getDataTableEntryCount(ExecutionContext arg0) throws Exception {
        int count = 0;
        try {
            count = RestClientAPIRepository.getInstance().getTableEntriesCount(arg0.getEntityName());
        } catch (Exception e) {
            throw new Exception("Unable to get count");
        }
        return count;
    }

    @Override
    public DataTableEntry getEntryByKey(String key, ExecutionContext arg1) throws Exception {
        List<DataTableEntry> listDataEntry = new ArrayList<>();
        JSONArray resultSet = null;
        try {
            resultSet = RestClientAPIRepository.getInstance().getEntryByKey(arg1.getEntityName(), key);
            if (resultSet == null)
                throw new Exception("No result from client side");
        } catch (Exception e) {
            return new DataTableEntry();
        }
        ArrayList<String> fieldNames = arg1.getDataShape().getFields().getNames();
        DataShapeDefinition dsf = arg1.getDataShape();
        listDataEntry = generateDataTableEntries(resultSet, fieldNames, dsf);
        return listDataEntry.get(0);
    }

    @Override
    public IDataEntryCloseableIterator<DataTableEntry> getEntryIterator(ExecutionContext arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public List<String> getEverything(ExecutionContext arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean keyExists(String key, ExecutionContext arg1) throws Exception {
        Boolean keyExist = false;
    	try {
    		keyExist = RestClientAPIRepository.getInstance().keyExists(arg1.getEntityName(),key);
        } catch (Exception e) {
            throw new Exception("Unable to get key existence");
        }
        return keyExist;
    }

    @Override
    public List<DataTableEntry> queryEntries(DataTableQueryCriteria queryCriteria, ExecutionContext arg1) throws Exception {
        List<DataTableEntry> listDataEntry = new ArrayList<>();
        JSONArray resultSet = null;
        Query query = queryCriteria.getQuery();
        String stringQuery = (query == null)? "{}":query.toJSON().toString();
        try {
            resultSet =
                RestClientAPIRepository.getInstance().queryTableEntries(arg1.getEntityName(), stringQuery);
            if (resultSet == null)
                throw new Exception("No result from client side");
        } catch (Exception e) {
            return listDataEntry;
        }
        ArrayList<String> fieldNames = arg1.getDataShape().getFields().getNames();
        DataShapeDefinition dsf = arg1.getDataShape();
        listDataEntry = generateDataTableEntries(resultSet, fieldNames, dsf);
        return listDataEntry;

    }

    private ArrayList<DataTableEntry> generateDataTableEntries(JSONArray resultSet, ArrayList<String> fieldNames, DataShapeDefinition dsf)
        throws Exception {
        ArrayList<DataTableEntry> alToReturn = new ArrayList<DataTableEntry>();
        for (int i = 0; i < resultSet.length(); i++) {
            JSONObject job = resultSet.getJSONObject(i);
            DataTableEntry dte = new DataTableEntry();
            dte.setKey(job.optString("key"));
            dte.setContainerID("contentID" + i);
            assignDataTableEntryValues(dte, job);
            ValueCollection vc = new ValueCollection();
            Iterator<String> it = job.keys();
            while (it.hasNext()) {
                String key = it.next();
                Object value = job.get(key);
                if(fieldNames.indexOf(key)==-1) {
                    for (int j = 0;j<fieldNames.size();j++) {
                        String a = fieldNames.get(j);
                        if(key.equals(a.toLowerCase()) ) {
                            key = a;
                        }
                    }
                }  
                vc.SetValue(dsf.getFieldDefinition(key), value);
            }
            dte.setValues(vc);
            alToReturn.add(dte);
        }
        return alToReturn;

    }
    
    private void assignDataTableEntryValues(DataTableEntry dte, JSONObject job) throws Exception {
        dte.assignValues(job.optString("source"), job.optString("sourceType"), new Location().fromString(job.optString("location")),
            new DateTime(job.opt("timestamp")), new TagCollection().fromString(job.optString("tags")));
        //Remove the assigned fields to get improve performance
        job.remove("source");
        job.remove("sourceType");
        job.remove("location");
        job.remove("timestamp");
        job.remove("tags");
        job.remove("key");

    }

}
