
package com.ptc.hackathon.mbps.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.thingworx.entities.utils.ThingUtilities;
import com.thingworx.things.Thing;

public class RestClientAPIRepository {

	private static String dataTableMicroserviceBaseUrl = "http://$Host:$Port/";

	private static final String createTableUrl = "CreateDataTableJson";

	private static final String queryDataTableEntries = "QueryDataTableEntries";

	private static final String addDataTableEntry = "AddDataTableEntry";
	
	private static final String keyExist = "KeyExists";
	
	private static final String updateDataTableEntries = "UpdateDataTableEntries";

	private static final String msHostThingName = "msHostThing";

	private static final String getEntriesByKey = "GetDataTableEntryByKey";

	private static final String removeDataTable = "RemoveDataTable";

	private static final String countDataTableEntries = "CountDataTableEntries";

	private static boolean msInitialized = false;

	private static RestClientAPIRepository restClientAPIRepository = null;

	private RestClientAPIRepository() {
	};

	private static final Logger _logger = LoggerFactory.getLogger(RestClientAPIRepository.class);

	public static RestClientAPIRepository getInstance() {
		initailizeMS();
		return new RestClientAPIRepository();
	}

	public JSONArray queryTableEntries(String dataTableName, String query) throws Exception {
		String url = dataTableMicroserviceBaseUrl + queryDataTableEntries;
		JSONObject params = new JSONObject();
		params.put("tableName", dataTableName);
		params.put("query", query);
		String result = executePost(url, params);
		return new JSONArray(result);
	}

	public JSONArray getEntryByKey(String dataTableName, String key) throws Exception {
		String url = dataTableMicroserviceBaseUrl + getEntriesByKey;
		JSONObject params = new JSONObject();
		params.put("tableName", dataTableName);
		params.put("key", key);
		String result = executePost(url, params);
		return new JSONArray(result);
	}

	public int getTableEntriesCount(String dataTableName) throws Exception {
		String url = dataTableMicroserviceBaseUrl + dataTableName + "/" + countDataTableEntries;
		JSONObject params = new JSONObject();
		params.put("tableName", dataTableName);
		String result = executeGet(url, params);
		JSONObject job = new JSONObject(result);
		return Integer.parseInt(job.optString("result"));
	}

	public void removeDataTable(String tableName) throws Exception {
		String url = dataTableMicroserviceBaseUrl + removeDataTable;
		JSONObject params = new JSONObject();
		params.put("dataTableName", tableName);
		executePost(url, params);

	}

	public void createTable(String dataTableName, String dataShapeName) throws Exception {
		String url = dataTableMicroserviceBaseUrl + createTableUrl;
		JSONObject params = new JSONObject();
		params.put("tableName", dataTableName);
		params.put("dataShapeName", dataShapeName);
		executePost(url, params);
	}

	public void updateTableEntry(String dataTableName, JSONObject updateEntry) throws Exception {
		String url = dataTableMicroserviceBaseUrl + dataTableName + "/" + updateDataTableEntries;		
		executePost(url, updateEntry);
		
	}
	
	public void addTableEntry(String dataTableName, String entryJSON) throws Exception {
		String url = dataTableMicroserviceBaseUrl + dataTableName + "/" + addDataTableEntry;
		JSONObject params = new JSONObject(entryJSON);
		executePost(url, params);
	}
	
	public Boolean keyExists(String dataTableName, String key) throws Exception {
		String url = dataTableMicroserviceBaseUrl + dataTableName + "/" + keyExist;
		JSONObject params = new JSONObject();
		params.put("primaryKey", key);
		return Boolean.parseBoolean(executePost(url, params));
	}

	private static void initailizeMS() {
		try {
			Thing msHostThing = ThingUtilities.findThingDirect(msHostThingName);
			String hostName = msHostThing.getPropertyValue("host").getStringValue();
			String port = msHostThing.getPropertyValue("port").getStringValue();
			dataTableMicroserviceBaseUrl = dataTableMicroserviceBaseUrl.replace("$Host", hostName);
			dataTableMicroserviceBaseUrl = dataTableMicroserviceBaseUrl.replace("$Port", port);
			System.out.println(hostName + "\n" + port);
			testConnection();
			msInitialized = true;
		} catch (Exception e) {
			return;
		}
	}

	private static void testConnection() throws Exception {
		URL url = new URL(dataTableMicroserviceBaseUrl);
		HttpURLConnection connection = (HttpURLConnection) url.openConnection();
		try {
			connection.connect();
		} catch (IOException e) {
			msInitialized = false;
			_logger.error(
					"Connection could not be established with the microservice datatable. Please check the host name and port in the 'msHostThing'\n URL: {}",
					dataTableMicroserviceBaseUrl);
			dataTableMicroserviceBaseUrl = "http://$Host:$Port/";
			throw e;
		}
	}

	private static String executePost(String urlString, JSONObject params) throws Exception {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("POST");
		OutputStream os = conn.getOutputStream();
		os.write(params.toString().getBytes("UTF-8"));
		os.close();
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));

		String output = br.readLine();
		conn.disconnect();
		return output;
	}

	private static String executeGet(String urlString, JSONObject params) throws Exception {
		URL url = new URL(urlString);
		HttpURLConnection conn = (HttpURLConnection) url.openConnection();
		conn.setDoOutput(true);
		conn.setDoInput(true);
		conn.setRequestProperty("Content-Type", "application/json");
		conn.setRequestProperty("Accept", "application/json");
		conn.setRequestMethod("GET");
		if (conn.getResponseCode() != 200) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}
		BufferedReader br = new BufferedReader(new InputStreamReader((conn.getInputStream())));
		String output = br.readLine();
		conn.disconnect();
		return output;
	}





}
