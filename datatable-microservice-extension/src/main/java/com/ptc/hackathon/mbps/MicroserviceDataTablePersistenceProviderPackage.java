package com.ptc.hackathon.mbps;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import org.joda.time.DateTime;

import com.thingworx.common.exceptions.DataAccessException;
import com.thingworx.persistence.ITransactionState;
import com.thingworx.persistence.common.SearchableDataServiceProvider;
import com.thingworx.persistence.provider.IDataProcessor;
import com.thingworx.persistence.provider.IDataProvider;
import com.thingworx.persistence.provider.IDataSearchProvider;
import com.thingworx.persistence.provider.IModelProviderFactory;
import com.thingworx.persistence.provider.IModelSearchProvider;
import com.thingworx.persistence.provider.IPersistenceConnection;
import com.thingworx.persistence.provider.IPersistenceInformationProvider;
import com.thingworx.persistence.provider.IPersistencePropertyProvider;
import com.thingworx.persistence.provider.ITagSearchProvider;
import com.thingworx.persistence.provider.PersistenceProvider;
import com.thingworx.persistence.provider.PersistenceProviderPackage;
import com.thingworx.persistence.search.IDataElement;
import com.thingworx.relationships.RelationshipTypes.ThingworxRelationshipTypes;
import com.thingworx.types.InfoTable;
import com.thingworx.types.TagCollection;
import com.thingworx.types.collections.AspectCollection;
import com.thingworx.types.collections.ConfigurationTableCollection;

public class MicroserviceDataTablePersistenceProviderPackage extends PersistenceProviderPackage {

    private ConcurrentHashMap<ThingworxRelationshipTypes, String> _supportedDataProviders;

    private ConcurrentHashMap<ThingworxRelationshipTypes, String> _supportedDataProcessors;

    private IDataSearchProvider _dataSearchProvider;

    public MicroserviceDataTablePersistenceProviderPackage() throws Exception {
        super();
        _supportedDataProviders = new ConcurrentHashMap<ThingworxRelationshipTypes, String>();
        _supportedDataProviders.put(ThingworxRelationshipTypes.ValueStreamEntry, "");
        _supportedDataProviders.put(ThingworxRelationshipTypes.StreamEntry, "");
        _supportedDataProviders.put(ThingworxRelationshipTypes.DataTableEntry, "");

        _supportedDataProcessors = new ConcurrentHashMap<ThingworxRelationshipTypes, String>();

        _dataSearchProvider = new IDataSearchProvider() {

            @Override
            public List<IDataElement> searchData(TagCollection arg0) throws DataAccessException {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public LinkedHashSet<IDataElement> queryDataElements(
                SearchableDataServiceProvider arg0,
                int arg1,
                String arg2,
                TagCollection arg3,
                LinkedHashSet<String> arg4,
                TagCollection arg5,
                DateTime arg6,
                DateTime arg7) throws Exception {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public LinkedHashSet<IDataElement> queryDataElements(
                SearchableDataServiceProvider arg0,
                int arg1,
                TagCollection arg2,
                LinkedHashSet<String> arg3,
                TagCollection arg4,
                DateTime arg5,
                DateTime arg6) throws Exception {
                // TODO Auto-generated method stub
                return null;
            }
        };
    }

    @Override
    public boolean canCreatePersistenceProvider() {
        // TODO Auto-generated method stub
        return true;
    }

    @Override
    public IDataProcessor getDataProcessor(ThingworxRelationshipTypes arg0, String arg1) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IDataProvider getDataProvider(ThingworxRelationshipTypes type, String persistenceProviderName) throws Exception {
        if (type.equals(ThingworxRelationshipTypes.DataTableEntry)) {
            return MicroserviceDataTableDataProvider.getInstance();
        }
        return null;
    }

    @Override
    public IDataSearchProvider getDataSearchProvider(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public AspectCollection getModelPersistenceProviderAspectCollection() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModelProviderFactory getModelProvider() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IModelSearchProvider getModelSearchProvider(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IPersistenceConnection getPersistenceConnection(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return new IPersistenceConnection() {

            @Override
            public void testConnectivity(ConfigurationTableCollection arg0, boolean arg1, InfoTable arg2) throws Exception {
                // TODO Auto-generated method stub

            }

            @Override
            public void shutdown() {

            }

            @Override
            public boolean supportsTransactions() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public boolean supportsParallelProcessing() {
                // TODO Auto-generated method stub
                return false;
            }

            @Override
            public void isConnected(ConfigurationTableCollection arg0, boolean arg1, InfoTable arg2) throws Exception {
                // TODO Auto-generated method stub

            }

            @Override
            public Object getConnection() throws Exception {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void disconnect() throws Exception {
                // TODO Auto-generated method stub

            }

            @Override
            public ITransactionState createTransaction() throws Exception {
                // TODO Auto-generated method stub
                return null;
            }

            @Override
            public void connect(ConfigurationTableCollection arg0) throws Exception {
                // TODO Auto-generated method stub

            }
        };
    }

    @Override
    public IPersistenceInformationProvider getPersistenceInformationProvider(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String getPersistenceProviderPackageName() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public IPersistencePropertyProvider getPropertyProvider() throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<ThingworxRelationshipTypes> getSupportedDataProcessors() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public Set<ThingworxRelationshipTypes> getSupportedDataProviders() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public ITagSearchProvider getTagSearchProvider(String arg0) throws Exception {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public boolean invalidateCaches(PersistenceProvider arg0) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }

    @Override
    public boolean isDataProcessingSupported(ThingworxRelationshipTypes arg0) throws Exception {
        // TODO Auto-generated method stub
        return false;
    }
}